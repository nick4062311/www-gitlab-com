---
features:
  secondary:
  - name: "Failing webhooks aren't automatically disabled for GitLab self-managed"
    available_in: [core, premium, ultimate]
    gitlab_com: false
    documentation_link: 'https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#failing-webhooks'
    reporter: m_frankiewicz
    stage: manage
    categories:
    - Webhooks
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/390157'
    description: |
     In GitLab 15.7, we introduced a feature to automatically disable project webhooks that fail consistently.

     In this release, we now automatically disable consistently failing group webhooks as well. This behavior is beneficial for large SaaS deployments such as
     GitLab.com, but might not be wanted by GitLab self-managed administrators.

     To give GitLab self-managed administrators more control, we don't automatically disable failing project or group webhooks by default for self-managed
     instances. To have consistently failing webhooks automatically disabled, instance administrators must enable the `auto_disabling_web_hooks`
     [feature flag](https://docs.gitlab.com/ee/administration/feature_flags.html).
